import {Button, Table} from "semantic-ui-react";

const ProcessRow = ({id, priority, createdAt, reloadProcessList}) => {
    const getPriorityColor = (priority) => {
        switch (priority) {
            case 'LOW':
                return '#fbbd08';
            case 'MEDIUM':
                return '#f2711c';
            case 'HIGH':
                return '#db2828';
            default:
                return 'black';
        }
    };

    const killProcess = (idToKill) => {
        return fetch(`/task-manager/processes/${idToKill}`, {
            method: 'DELETE'
        })
            .then((res) => {
                if (res.status !== 200) {
                    return res.json().then((data) => {
                        if (data && data.error) {
                            console.log(data);
                            return data;
                        } else {
                            return null;
                        }
                    }, (error) => {
                        console.log(error);
                        return "Service not available! Please try again later.";
                    })
                } else {
                    return null;
                }
            });
    };

    const handleKill = () => {
        killProcess(id).then((error) => {
            if (error === null) {
                reloadProcessList(true);
            } else {
                alert(JSON.stringify(error));
            }
        })
    }

    return (
        <Table.Row>
            <Table.Cell>{id}</Table.Cell>
            <Table.Cell style={{
                color: getPriorityColor(priority),
                fontWeight: 'bold'
            }}>{priority}</Table.Cell>
            <Table.Cell>{createdAt}</Table.Cell>
            <Table.Cell>
                <Button inverted color='red' floated='right' onClick={handleKill}>
                    Kill
                </Button>
            </Table.Cell>
        </Table.Row>
    );
};

export default ProcessRow;