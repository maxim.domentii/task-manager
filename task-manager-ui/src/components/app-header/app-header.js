import {Divider, Header} from "semantic-ui-react";

const AppHeader = ({highProcesses, mediumProcesses, lowProcesses}) => {
    return (
        <>
            <div style={{margin: '1rem 0'}}>
                <Header as='h1'>Task Manager</Header>
                <Header as='h2' style={{
                    fontSize: '1.2rem',
                    color: 'grey'
                }}>{highProcesses} HIGH, {mediumProcesses} MEDIUM and {lowProcesses} LOW processes</Header>
            </div>
            <Divider/>
        </>
    )
};

export default AppHeader;