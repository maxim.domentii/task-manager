import {Container, Loader, Table} from "semantic-ui-react";
import ProcessRow from "../process-row";
import FiltersRow from "../filters-row";
import AppHeader from "../app-header";
import AddKillProcessesRow from "../add-kill-processes-row";
import useListProcessesApi from "./hooks/useListProcessesApi";
import GenericErrorBoundary from "../generic-error-boundary";

const App = () => {
    const {processes, isProcessesLoaded, error, setReload, setSort} = useListProcessesApi();

    const processRows = () => {
        if (!isProcessesLoaded){
            return  [];
        }

        if (error) {
            return [];
        }

        return processes.map(({id, priority, createdAt}) => {
            return (
                <ProcessRow key={id} id={id} priority={priority} createdAt={createdAt} reloadProcessList={setReload}/>
            );
        });
    };

    const processesCountByPriority = (priorityBy) => {
        if (!isProcessesLoaded){
            return 0;
        }

        if (error) {
            return 0;
        }

        return processes.filter(({priority}) => priority === priorityBy).length;
    };

    return (
        <Container>
            <GenericErrorBoundary error={error}>
                <AppHeader
                    highProcesses={processesCountByPriority('HIGH')}
                    mediumProcesses={processesCountByPriority('MEDIUM')}
                    lowProcesses={processesCountByPriority('LOW')}
                />
                <Table unstackable>
                    <Table.Header>
                        <FiltersRow sortCallback={setSort}/>
                    </Table.Header>

                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>PID</Table.HeaderCell>
                            <Table.HeaderCell>PRIORITY</Table.HeaderCell>
                            <Table.HeaderCell>CREATION TIME</Table.HeaderCell>
                            <Table.HeaderCell/>
                        </Table.Row>
                    </Table.Header>

                    {isProcessesLoaded ?
                        <Table.Body>
                            {processRows()}
                        </Table.Body>
                        : <Loader active inline='centered'/>}

                    <Table.Footer fullWidth>
                        <AddKillProcessesRow reloadProcessList={setReload}/>
                    </Table.Footer>
                </Table>
            </GenericErrorBoundary>
        </Container>
    )
};

export default App;