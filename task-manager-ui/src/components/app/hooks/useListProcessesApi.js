import {useEffect, useState} from "react";

const useListProcessesApi = () => {
    const [reload, setReload] = useState(false);
    const [processes, setProcesses] = useState([]);
    const [isProcessesLoaded, setIsProcessesLoaded] = useState(false);
    const [error, setError] = useState(null);
    const [sort, setSort] = useState({
        property: 'CREATION_TIME',
        direction: 'DESC'
    })

    useEffect(() => {
        setIsProcessesLoaded(false)
        const params = new URLSearchParams(sort);
        fetch(`/task-manager/processes?${params}`)
            .then((res) =>res.json())
            .then(
                (data) => {
                    if (data.error) {
                        console.log(data);
                        setProcesses({});
                        setError(data);
                        setIsProcessesLoaded(true);
                    } else {
                        setProcesses(data);
                        setIsProcessesLoaded(true);
                        setError(null);
                    }
                    setReload(false);
                },
                (error) => {
                    console.log(error);
                    setProcesses({});
                    setError(error);
                    setIsProcessesLoaded(true);
                    setReload(false);
                });

        // cleanup function
        return () => {
        };
    }, [reload, sort]);

    return {
        processes,
        isProcessesLoaded,
        error,
        setReload,
        setSort
    }
};

export default useListProcessesApi;