import {Button, Table} from "semantic-ui-react";
import AddNewProcess from "../add-new-process";

const AddKillProcessesRow = ({reloadProcessList}) => {
    const killProcesses = (priority) => {
        const url = priority ? `/task-manager/processes?${new URLSearchParams({
            priority: priority
        })}` : '/task-manager/processes';

        return fetch(url, {
            method: 'DELETE'
        })
            .then((res) => {
                if (res.status !== 200) {
                    return res.json().then((data) => {
                        if (data && data.error) {
                            console.log(data);
                            return data;
                        } else {
                            return null;
                        }
                    }, (error) => {
                        console.log(error);
                        return "Service not available! Please try again later.";
                    })
                } else {
                    return null;
                }
            });
    };

    const handleKill = (priority) => {
        killProcesses(priority).then((error) => {
            if (error === null) {
                reloadProcessList(true);
            } else {
                alert(JSON.stringify(error));
            }
        })
    }

    return (
        <Table.Row>
            <Table.HeaderCell colSpan='4'>
                <Button color='yellow' floated='right' onClick={() => handleKill('LOW')}>
                    Kill LOW processes
                </Button>
                <Button color='orange' floated='right' onClick={() => handleKill('MEDIUM')}>
                    Kill MEDIUM processes
                </Button>
                <Button color='red' floated='right' onClick={() => handleKill('HIGH')}>
                    Kill HIGH processes
                </Button>
                <Button secondary floated='right' onClick={() => handleKill()}>
                    Kill ALL
                </Button>
                <AddNewProcess reloadProcessList={reloadProcessList}/>
            </Table.HeaderCell>
        </Table.Row>
    )
};

export default AddKillProcessesRow;