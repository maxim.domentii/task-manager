import {Button, Form, Modal} from "semantic-ui-react";
import {useState} from 'react';

const AddNewProcess = ({reloadProcessList}) => {
    const [open, setOpen] = useState(false);
    const [form, setForm] = useState({
        id: '',
        priority: 'LOW'
    })

    const addProcess = (request) => {
        return fetch('/task-manager/process', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify(request)
        })
            .then((res) => res.json())
            .then(
                (data) => {
                    if (data.error) {
                        console.log(data);
                        return data;
                    } else {
                        return null;
                    }
                },
                (error) => {
                    console.log(error);
                    return "Service not available! Please try again later.";
                });
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        addProcess(form).then((error) => {
            if (error === null) {
                reloadProcessList(true);
                setOpen(false);
                setForm({
                    id: '',
                    priority: 'LOW'
                })
            } else {
                alert(JSON.stringify(error));
            }
        });
    };

    const handleInputChange = ({target: {name, value}}) => {
        setForm({...form, [name]: value})
    }

    return (
        <Modal
            centered={false}
            size='mini'
            as={Form}
            onSubmit={e => handleSubmit(e)}
            closeIcon
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            open={open}
            trigger={<Button primary floated='left'>Add new process</Button>}
        >
            <Modal.Header>Add new process</Modal.Header>
            <Modal.Content>
                <Form.Field>
                    <label>PID</label>
                    <input name="id"
                           placeholder='PID'
                           required
                           value={form.id}
                           onChange={handleInputChange}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Priority</label>
                    <select name="priority" value={form.priority} onChange={handleInputChange}>
                        <option value="LOW">LOW</option>
                        <option value="MEDIUM">MEDIUM</option>
                        <option value="HIGH">HIGH</option>
                    </select>
                </Form.Field>
            </Modal.Content>
            <Modal.Actions>
                <Button secondary onClick={() => setOpen(false)}>
                    Cancel
                </Button>
                <Button primary type='submit'>
                    Submit
                </Button>
            </Modal.Actions>
        </Modal>
    )
};

export default AddNewProcess;