import {Button, Table} from "semantic-ui-react";
import {useState} from 'react';

const FiltersRow = ({sortCallback}) => {
    const [ID, setSortById] = useState('up');
    const [PRIORITY, setSortByPriority] = useState('up');
    const [CREATION_TIME, setSortByTime] = useState('up');

    const sortBy = {
        ID,
        PRIORITY,
        CREATION_TIME
    };

    const sortBySetters = {
        ID: setSortById,
        PRIORITY: setSortByPriority,
        CREATION_TIME: setSortByTime
    };

    const handleSortClick = (event) => {
        sortBySetters["ID"]('up');
        sortBySetters["PRIORITY"]('up');
        sortBySetters["CREATION_TIME"]('up');
        sortBySetters[event.target.name](sortBy[event.target.name] === 'up' ? 'down' : 'up');
        sortCallback({
            property: event.target.name,
            direction: sortBy[event.target.name] === 'up' ? 'DESC' : 'ASC'
        })
    };

    return (
        <Table.Row>
            <Table.HeaderCell>Filter by:</Table.HeaderCell>
            <Table.HeaderCell colSpan='3'>
                <Button.Group floated='right'>
                    <Button content='PID' icon={`sort amount ${ID}`} labelPosition='right'
                            name='ID'
                            onClick={handleSortClick}
                    />
                    <Button content='PRIORITY' icon={`sort amount ${PRIORITY}`} labelPosition='right'
                            name='PRIORITY'
                            onClick={handleSortClick}
                    />
                    <Button content='CREATION TIME' icon={`sort amount ${CREATION_TIME}`} labelPosition='right'
                            name='CREATION_TIME'
                            onClick={handleSortClick}
                    />
                </Button.Group>
            </Table.HeaderCell>
        </Table.Row>
    );
};

export default FiltersRow;