import {Message} from "semantic-ui-react";

const GenericErrorBoundary = ({error, children}) => {
    if (error) {
        return (
            <Message negative>
                <Message.Header>Service Unavailable! Please try again later.</Message.Header>
                <p>{JSON.stringify(error)}</p>
            </Message>
        );
    } else {
        return children;
    }
};

export default GenericErrorBoundary;
