# Tasm Manager UI

Task Manager UI application build with ReactJS and Hooks.
It uses the Task Manager Service API.

## Build
To build the package run:
```shell
npm install
```

## Run
To run the package run:
```shell
npm start
```

## Usage
After application is started you can access it locally at http://localhost:3000