## Task Manager

Under this repository you will find two software components:
* task-manager-service: service component that implements the business logic and expose it as a Rest API
* task-manager-ui: UI component that uses the Task Manager Service API
More details you can find under README.md files part of each ot these two packages.
  
**Note**: You can try out service component without running UI. 
Check details in task-manager-service/README.md file.
  
## Prerequisites
* java8
* maven
* npm

## Build and run
```shell
# build and run the service
cd task-manage-service
mvn clean install
mvn spring-boot:run

# build and run the UI
cd ../task-manager-ui 
npm install
npm start
```

## Usage
Once both service and UI are up and running you can access UI at http://localhost:3000 and try it out.