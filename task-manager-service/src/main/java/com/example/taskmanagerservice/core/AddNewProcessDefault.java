package com.example.taskmanagerservice.core;

import com.example.taskmanagerservice.exception.NotAcceptableException;
import com.example.taskmanagerservice.model.Process;

import java.util.Map;

public class AddNewProcessDefault extends AddNewProcessFactory {

    @Override
    protected void checkCapacity(Process newProcess, int capacity, Map<String, Process> processMap) {
        if (processMap.size() == capacity){
            throw new NotAcceptableException("No more capacity! Kill a process first.");
        }
    }
}
