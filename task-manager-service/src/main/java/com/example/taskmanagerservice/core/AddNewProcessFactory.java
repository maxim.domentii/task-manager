package com.example.taskmanagerservice.core;

import com.example.taskmanagerservice.exception.BadRequestException;
import com.example.taskmanagerservice.model.Process;

import java.util.Map;

public abstract class AddNewProcessFactory {
    public Process add(Process newProcess, int capacity, Map<String, Process> processMap) {
        String id = newProcess.getId();
        if (processMap.containsKey(id)){
            throw new BadRequestException("A process with id " + id + " exist already.");
        }

        checkCapacity(newProcess, capacity, processMap);

        processMap.put(id, newProcess);
        return newProcess;
    }

    protected abstract void checkCapacity(Process process, int capacity, Map<String, Process> processMap);
}
