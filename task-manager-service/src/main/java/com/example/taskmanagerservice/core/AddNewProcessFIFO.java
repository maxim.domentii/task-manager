package com.example.taskmanagerservice.core;

import com.example.taskmanagerservice.model.Process;

import java.util.Map;

public class AddNewProcessFIFO extends AddNewProcessFactory {

    @Override
    protected void checkCapacity(Process newProcess, int capacity, Map<String, Process> processMap) {
        if (processMap.size() == capacity){
            Process eldestProcess = processMap.entrySet().iterator().next().getValue();
            processMap.remove(eldestProcess.getId());
            eldestProcess.kill();
        }
    }
}
