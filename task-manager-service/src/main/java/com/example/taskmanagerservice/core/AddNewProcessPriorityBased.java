package com.example.taskmanagerservice.core;

import com.example.taskmanagerservice.exception.NotAcceptableException;
import com.example.taskmanagerservice.model.Process;

import java.util.Iterator;
import java.util.Map;

public class AddNewProcessPriorityBased extends AddNewProcessFactory {

    @Override
    protected void checkCapacity(Process newProcess, int capacity, Map<String, Process> processMap) {
        if (processMap.size() == capacity){
            Iterator<Map.Entry<String, Process>> iterator = processMap.entrySet().iterator();
            Process eldestProcess = null;
            while (iterator.hasNext()) {
                Process process = iterator.next().getValue();
                if (process.getPriority().getOrder() < newProcess.getPriority().getOrder()){
                    eldestProcess = process;
                    break;
                }
            }

            if (eldestProcess != null){
                processMap.remove(eldestProcess.getId());
                eldestProcess.kill();
            } else {
                throw new NotAcceptableException("No more capacity! There is no older process with lower priority. " +
                        "Kill a process first.");
            }
        }
    }
}
