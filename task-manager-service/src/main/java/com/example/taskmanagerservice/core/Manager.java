package com.example.taskmanagerservice.core;

import com.example.taskmanagerservice.exception.NotFoundException;
import com.example.taskmanagerservice.model.Process;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class Manager {

    private static final String FIFO = "FIFO";
    private static final String PRIORITY_BASED = "PriorityBased";

    private int capacity;
    private Map<String, Process> processMap;
    private AddNewProcessFactory addNewProcessFactory;

    public Manager(@Value("${task.manager.capacity:10}") Integer capacity,
                   @Value("${task.manager.factory-method:DEFAULT}") String factoryMethod){

        log.info("Task Manager initialized with capacity={}, factory-method for add operation={}", capacity, factoryMethod);

        this.capacity = capacity;
        this.processMap = Collections.synchronizedMap(new LinkedHashMap<>());

        switch (factoryMethod){
            case FIFO:
                addNewProcessFactory = new AddNewProcessFIFO();
                break;
            case PRIORITY_BASED:
                addNewProcessFactory = new AddNewProcessPriorityBased();
                break;
            default:
                addNewProcessFactory = new AddNewProcessDefault();
        }
    }

    public Process add(Process process){
        return addNewProcessFactory.add(process, capacity, processMap);
    }

    public List<Process> listProcesses(){
        return new ArrayList<>(processMap.values());
    }

    public void killProcessById(String id){
        if (!processMap.containsKey(id)){
            throw new NotFoundException("Process with id " + id + " not found.");
        }

        Process process = processMap.remove(id);
        process.kill();
    }

    public void killProcessesByPriority(Process.Priority priority){
        List<String> processIdsWithNeededPriority = processMap.values().stream()
                .filter(process -> process.getPriority().equals(priority))
                .map(Process::getId).collect(Collectors.toList());

        for (String id : processIdsWithNeededPriority){
            killProcessById(id);
        }
    }

    public void killAllProcesses(){
        List<String> processIds = processMap.values().stream().map(Process::getId).collect(Collectors.toList());

        for (String id : processIds){
            killProcessById(id);
        }
    }
}
