package com.example.taskmanagerservice.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@Getter
@EqualsAndHashCode
@ToString
@Slf4j
public final class Process {
    private final String id;
    private final Priority priority;
    @JsonSerialize(using = ToStringSerializer.class)
    private final LocalDateTime createdAt;

    public Process(String id, Priority priority, LocalDateTime createdAt){
        this.id = id;
        this.priority = priority;
        this.createdAt = createdAt;
    }

    public void kill() {
        log.info("Process with id {} has been killed.", id);
    }

    public enum Priority {
        LOW(1), MEDIUM(2), HIGH(3);

        private int order;

        Priority(int priority){
            this.order = priority;
        }

        public int getOrder() {
            return order;
        }
    }
}
