package com.example.taskmanagerservice.controller;

import com.example.taskmanagerservice.model.Process;
import com.example.taskmanagerservice.request.ProcessRequest;
import com.example.taskmanagerservice.request.Sort;
import com.example.taskmanagerservice.service.ITaskManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/task-manager")
public class TaskManagerController {

    private ITaskManagerService taskManagerService;

    @Autowired
    public TaskManagerController(ITaskManagerService taskManagerService){
        this.taskManagerService = taskManagerService;
    }

    @PostMapping(path = "/process", produces = "application/json", consumes = "application/json")
    public ResponseEntity<Process> add(@RequestBody ProcessRequest processRequest) {
        Process process = taskManagerService.add(processRequest);
        return new ResponseEntity<>(process, HttpStatus.CREATED);
    }

    @GetMapping(path = "/processes", produces = "application/json")
    public ResponseEntity<List<Process>> list(@ModelAttribute Sort sort) {
        List<Process> processList = taskManagerService.listProcesses(sort);
        return new ResponseEntity<>(processList, HttpStatus.OK);
    }

    @DeleteMapping(path = "/processes/{id}", produces = "application/json")
    public ResponseEntity<Void> list(@PathVariable("id") String id) {
        taskManagerService.killProcessById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(path = "/processes", produces = "application/json")
    public ResponseEntity<Void> list(@RequestParam(name = "priority", required = false) Process.Priority priority) {
        if (priority != null){
            taskManagerService.killProcessesByPriority(priority);
        } else {
            taskManagerService.killAllProcesses();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
