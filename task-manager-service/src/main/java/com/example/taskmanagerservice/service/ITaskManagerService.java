package com.example.taskmanagerservice.service;

import com.example.taskmanagerservice.model.Process;
import com.example.taskmanagerservice.request.ProcessRequest;
import com.example.taskmanagerservice.request.Sort;

import java.util.List;

public interface ITaskManagerService {

    Process add(ProcessRequest processRequest);
    List<Process> listProcesses(Sort sort);
    void killProcessById(String id);
    void killProcessesByPriority(Process.Priority priority);
    void killAllProcesses();

}
