package com.example.taskmanagerservice.service;

import com.example.taskmanagerservice.core.Manager;
import com.example.taskmanagerservice.model.Process;
import com.example.taskmanagerservice.request.ProcessRequest;
import com.example.taskmanagerservice.request.Sort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.taskmanagerservice.request.Sort.Direction.ASC;

@Service
public class TaskManagerService implements ITaskManagerService {

    private Manager manager;

    @Autowired
    public TaskManagerService(Manager manager){
        this.manager = manager;
    }

    @Override
    public Process add(ProcessRequest processRequest) {
        return manager.add(new Process(processRequest.getId(), processRequest.getPriority(), LocalDateTime.now()));
    }

    @Override
    public List<Process> listProcesses(Sort sort) {
        List<Process> processList = new ArrayList<>(manager.listProcesses());
        List<Process> sortedProcessList;
        if (sort == null || sort.getProperty() == null){
            sortedProcessList = processList.stream().sorted(Comparator.comparing(Process::getCreatedAt).reversed())
                    .collect(Collectors.toList());
        } else {
            switch (sort.getProperty()) {
                case ID:
                    sortedProcessList = ASC.equals(sort.getDirection()) ?
                            processList.stream().sorted(Comparator.comparing(Process::getId)).collect(Collectors.toList())
                            : processList.stream().sorted(Comparator.comparing(Process::getId).reversed())
                            .collect(Collectors.toList());
                    break;
                case PRIORITY:
                    sortedProcessList = ASC.equals(sort.getDirection()) ?
                            processList.stream().sorted(Comparator.comparing(Process::getPriority)).collect(Collectors.toList())
                            : processList.stream().sorted(Comparator.comparing(Process::getPriority).reversed())
                            .collect(Collectors.toList());
                    break;
                default:
                    sortedProcessList = ASC.equals(sort.getDirection()) ?
                            processList.stream().sorted(Comparator.comparing(Process::getCreatedAt)).collect(Collectors.toList())
                            : processList.stream().sorted(Comparator.comparing(Process::getCreatedAt).reversed())
                            .collect(Collectors.toList());
            }
        }
        return sortedProcessList;
    }

    @Override
    public void killProcessById(String id) {
        manager.killProcessById(id);
    }

    @Override
    public void killProcessesByPriority(Process.Priority priority) {
        manager.killProcessesByPriority(priority);
    }

    @Override
    public void killAllProcesses() {
        manager.killAllProcesses();
    }
}
