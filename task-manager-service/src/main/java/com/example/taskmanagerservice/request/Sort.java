package com.example.taskmanagerservice.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sort {

    private Direction direction;
    private Property property;

    public Sort() {

    }

    public enum Direction {
        ASC, DESC;
    }

    public enum Property {
        CREATION_TIME, PRIORITY, ID;
    }
}
