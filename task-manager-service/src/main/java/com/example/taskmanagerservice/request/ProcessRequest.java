package com.example.taskmanagerservice.request;

import com.example.taskmanagerservice.model.Process;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessRequest {

    private String id;
    private Process.Priority priority;

    public ProcessRequest() {

    }
}
