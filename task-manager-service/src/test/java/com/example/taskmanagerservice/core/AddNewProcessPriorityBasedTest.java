package com.example.taskmanagerservice.core;

import com.example.taskmanagerservice.exception.NotAcceptableException;
import com.example.taskmanagerservice.model.Process;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.Fail.fail;

public class AddNewProcessPriorityBasedTest {

    private AddNewProcessFactory newProcessFactory = new AddNewProcessPriorityBased();

    @Test
    public void checkCapacityWhenFullRemovesOldestWithLowerPriority() {
        // given
        int capacity = 2;
        Map<String, Process> processMap = new LinkedHashMap<>();
        processMap.put("1", new Process("1", Process.Priority.HIGH, LocalDateTime.now()));
        processMap.put("2", new Process("2", Process.Priority.LOW, LocalDateTime.now()));
        Process newProcess = new Process("3", Process.Priority.MEDIUM, LocalDateTime.now());

        // when
        newProcessFactory.checkCapacity(newProcess, capacity, processMap);

        // then
        assertThat(processMap.size()).isEqualTo(1);
        assertThat(new ArrayList<>(processMap.keySet())).asList().containsExactly("1");
    }

    @Test
    public void checkCapacityWhenFullAndNoProcessWithLowerPriorityThrowException() {
        // given
        int capacity = 2;
        Map<String, Process> processMap = new LinkedHashMap<>();
        processMap.put("1", new Process("1", Process.Priority.HIGH, LocalDateTime.now()));
        processMap.put("2", new Process("2", Process.Priority.HIGH, LocalDateTime.now()));
        Process newProcess = new Process("3", Process.Priority.HIGH, LocalDateTime.now());

        try {
            // when
            newProcessFactory.checkCapacity(newProcess, capacity, processMap);
            fail("Should fail!");
        } catch (NotAcceptableException ex) {
            // then
            Assertions.assertThat(ex.getMessage()).isEqualTo("No more capacity! There is no older process with lower " +
                    "priority. Kill a process first.");
        }
    }

    @Test
    public void checkCapacity() {
        // given
        int capacity = 2;
        Map<String, Process> processMap = new LinkedHashMap<>();
        processMap.put("1", new Process("1", Process.Priority.HIGH, LocalDateTime.now()));
        Process newProcess = new Process("2", Process.Priority.MEDIUM, LocalDateTime.now());

        // when
        newProcessFactory.checkCapacity(newProcess, capacity, processMap);

        // then
        assertThat(processMap.size()).isEqualTo(1);
        assertThat(new ArrayList<>(processMap.keySet())).asList().containsExactly("1");
    }
}