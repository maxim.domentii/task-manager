package com.example.taskmanagerservice.core;

import com.example.taskmanagerservice.exception.BadRequestException;
import com.example.taskmanagerservice.exception.NotAcceptableException;
import com.example.taskmanagerservice.exception.NotFoundException;
import com.example.taskmanagerservice.model.Process;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;

public class ManagerTest {

    @Test
    public void addWithSameId() {
        // given
        Manager manager = new Manager(2, "");
        Process process = new Process("1", Process.Priority.LOW, LocalDateTime.now());
        manager.add(process);
        Process newProcess = new Process("1", Process.Priority.HIGH, LocalDateTime.now());


        try {
            // when
            manager.add(newProcess);
            fail("Should fail!");
        } catch (BadRequestException ex) {
            // then
            assertThat(ex.getMessage()).isEqualTo("A process with id 1 exist already.");
        }
    }

    @Test
    public void addWhenThereIsMoreCapacity() {
        // given
        Manager manager = new Manager(1, "");
        Process newProcess = new Process("1", Process.Priority.HIGH, LocalDateTime.now());

        // when
        manager.add(newProcess);

        // then
        assertThat(manager.listProcesses().size()).isEqualTo(1);
        assertThat(manager.listProcesses().get(0).getId()).isEqualTo("1");
    }

    @Test
    public void addWhenThereNoMoreCapacityWithDefaultFactory() {
        // given
        Manager manager = new Manager(1, "");
        manager.add(new Process("1", Process.Priority.HIGH, LocalDateTime.now()));
        Process newProcess = new Process("2", Process.Priority.HIGH, LocalDateTime.now());

        try {
            // when
            manager.add(newProcess);
            fail("Should fail!");
        } catch (NotAcceptableException ex) {
            // then
            assertThat(ex.getMessage()).isEqualTo("No more capacity! Kill a process first.");
        }
    }

    @Test
    public void addWhenThereNoMoreCapacityWithFIFOFactory() {
        // given
        Manager manager = new Manager(1, "FIFO");
        manager.add(new Process("1", Process.Priority.HIGH, LocalDateTime.now()));
        Process newProcess = new Process("2", Process.Priority.HIGH, LocalDateTime.now());

        // when
        manager.add(newProcess);

        // then
        assertThat(manager.listProcesses().size()).isEqualTo(1);
        assertThat(manager.listProcesses()).asList().containsExactly(newProcess);
    }

    @Test
    public void addWhenThereNoMoreCapacityWithPriorityBasedFactory() {
        // given
        Manager manager = new Manager(2, "PriorityBased");
        Process process1 = new Process("1", Process.Priority.HIGH, LocalDateTime.now());
        manager.add(process1);
        Process process2 = new Process("2", Process.Priority.LOW, LocalDateTime.now());
        manager.add(process2);
        Process newProcess = new Process("3", Process.Priority.HIGH, LocalDateTime.now());

        // when
        manager.add(newProcess);

        // then
        assertThat(manager.listProcesses().size()).isEqualTo(2);
        assertThat(manager.listProcesses()).asList().containsExactly(process1, newProcess);
    }

    @Test
    public void listProcesses() {
        // given
        Manager manager = new Manager(2, "PriorityBased");
        Process process1 = new Process("1", Process.Priority.HIGH, LocalDateTime.now());
        manager.add(process1);
        Process process2 = new Process("2", Process.Priority.LOW, LocalDateTime.now());
        manager.add(process2);

        // when
        List<Process> result = manager.listProcesses();

        // then
        assertThat(result.size()).isEqualTo(2);
        assertThat(result).asList().containsExactly(process1, process2);
    }

    @Test
    public void killProcessByMissingIdThrowsException() {
        // given
        Manager manager = new Manager(1, "");
        Process process1 = new Process("1", Process.Priority.HIGH, LocalDateTime.now());
        manager.add(process1);

        try {
            // when
            manager.killProcessById("2");
            fail("Should fail!");
        } catch (NotFoundException ex) {
            // then
            assertThat(ex.getMessage()).isEqualTo("Process with id 2 not found.");
        }
    }

    @Test
    public void killProcessById() {
        // given
        Manager manager = new Manager(1, "");
        Process process1 = new Process("1", Process.Priority.HIGH, LocalDateTime.now());
        manager.add(process1);

        // when
        manager.killProcessById("1");

        // then
        assertThat(manager.listProcesses().size()).isEqualTo(0);
    }

    @Test
    public void killProcessesByPriority() {
        // given
        Manager manager = new Manager(2, "");
        Process process1 = new Process("1", Process.Priority.HIGH, LocalDateTime.now());
        manager.add(process1);
        Process process2 = new Process("2", Process.Priority.LOW, LocalDateTime.now());
        manager.add(process2);

        // when
        manager.killProcessesByPriority(Process.Priority.LOW);

        // then
        assertThat(manager.listProcesses().size()).isEqualTo(1);
        assertThat(manager.listProcesses().get(0).getPriority()).isEqualTo(Process.Priority.HIGH);
    }

    @Test
    public void killAllProcesses() {
        // given
        Manager manager = new Manager(2, "");
        Process process1 = new Process("1", Process.Priority.HIGH, LocalDateTime.now());
        manager.add(process1);
        Process process2 = new Process("2", Process.Priority.LOW, LocalDateTime.now());
        manager.add(process2);

        // when
        manager.killAllProcesses();

        // then
        assertThat(manager.listProcesses().size()).isEqualTo(0);
    }
}