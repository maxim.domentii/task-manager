package com.example.taskmanagerservice.core;

import com.example.taskmanagerservice.model.Process;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AddNewProcessFIFOTest {

    private AddNewProcessFactory newProcessFactory = new AddNewProcessFIFO();

    @Test
    public void checkCapacityWhenFullRemovesOldest() {
        // given
        int capacity = 2;
        Map<String, Process> processMap = new LinkedHashMap<>();
        processMap.put("1", new Process("1", Process.Priority.HIGH, LocalDateTime.now()));
        processMap.put("2", new Process("2", Process.Priority.LOW, LocalDateTime.now()));

        // when
        newProcessFactory.checkCapacity(null, capacity, processMap);

        // then
        assertThat(processMap.size()).isEqualTo(1);
        assertThat(new ArrayList<>(processMap.keySet())).asList().containsExactly("2");
    }

    @Test
    public void checkCapacity() {
        // given
        int capacity = 2;
        Map<String, Process> processMap = new LinkedHashMap<>();
        processMap.put("1", new Process("1", Process.Priority.HIGH, LocalDateTime.now()));

        // when
        newProcessFactory.checkCapacity(null, capacity, processMap);

        // then
        assertThat(processMap.size()).isEqualTo(1);
        assertThat(new ArrayList<>(processMap.keySet())).asList().containsExactly("1");
    }
}