package com.example.taskmanagerservice.core;

import com.example.taskmanagerservice.exception.NotAcceptableException;
import com.example.taskmanagerservice.model.Process;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;

public class AddNewProcessDefaultTest {

    private AddNewProcessFactory newProcessFactory = new AddNewProcessDefault();

    @Test
    public void checkCapacityThrowsExceptionWhenNoMoreCapacity() {
        // given
        int capacity = 1;
        Map<String, Process> processMap = Collections.singletonMap("id",
                new Process("id", Process.Priority.HIGH, LocalDateTime.now()));

        try {
            // when
            newProcessFactory.checkCapacity(null, capacity, processMap);
            fail("Should fail!");
        } catch (NotAcceptableException ex) {
            // then
            assertThat(ex.getMessage()).isEqualTo("No more capacity! Kill a process first.");
        }
    }

    @Test
    public void checkCapacity() {
        // given
        int capacity = 1;
        Map<String, Process> processMap = Collections.emptyMap();

        // when
        newProcessFactory.checkCapacity(null, capacity, processMap);
    }
}