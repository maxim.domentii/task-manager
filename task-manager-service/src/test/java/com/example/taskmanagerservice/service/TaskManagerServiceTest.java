package com.example.taskmanagerservice.service;

import com.example.taskmanagerservice.core.Manager;
import com.example.taskmanagerservice.model.Process;
import com.example.taskmanagerservice.request.Sort;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TaskManagerServiceTest {

    private static final Process process1 = new Process("2", Process.Priority.MEDIUM, LocalDateTime.now());
    private static final Process process2 = new Process("3", Process.Priority.HIGH, LocalDateTime.now().plusSeconds(5));
    private static final Process process3 = new Process("1", Process.Priority.LOW, LocalDateTime.now().minusSeconds(5));

    private ITaskManagerService taskManagerService;

    @Mock
    private Manager manager;

    @Before
    public void setUp() {
        this.taskManagerService = new TaskManagerService(manager);
        when(manager.listProcesses()).thenReturn(Arrays.asList(process1, process2, process3));
    }

    @Test
    public void listProcessesSortedByDefault() {
        // when
        List<Process> result = taskManagerService.listProcesses(null);

        // then
        assertThat(result).asList().containsExactly(process2, process1, process3);
    }

    @Test
    public void listProcessesSortedByCreationTimeAsc() {
        // when
        Sort sort = new Sort();
        sort.setDirection(Sort.Direction.ASC);
        sort.setProperty(Sort.Property.CREATION_TIME);
        List<Process> result = taskManagerService.listProcesses(sort);

        // then
        assertThat(result).asList().containsExactly(process3, process1, process2);
    }

    @Test
    public void listProcessesSortedById() {
        // when
        Sort sort = new Sort();
        sort.setProperty(Sort.Property.ID);
        List<Process> result = taskManagerService.listProcesses(sort);

        // then
        assertThat(result).asList().containsExactly(process2, process1, process3);
    }

    @Test
    public void listProcessesSortedByIdAsc() {
        // when
        Sort sort = new Sort();
        sort.setDirection(Sort.Direction.ASC);
        sort.setProperty(Sort.Property.ID);
        List<Process> result = taskManagerService.listProcesses(sort);

        // then
        assertThat(result).asList().containsExactly(process3, process1, process2);
    }

    @Test
    public void listProcessesSortedByPriority() {
        // when
        Sort sort = new Sort();
        sort.setProperty(Sort.Property.PRIORITY);
        List<Process> result = taskManagerService.listProcesses(sort);

        // then
        assertThat(result).asList().containsExactly(process2, process1, process3);
    }

    @Test
    public void listProcessesSortedByPriorityAsc() {
        // when
        Sort sort = new Sort();
        sort.setDirection(Sort.Direction.ASC);
        sort.setProperty(Sort.Property.PRIORITY);
        List<Process> result = taskManagerService.listProcesses(sort);

        // then
        assertThat(result).asList().containsExactly(process3, process1, process2);
    }
}