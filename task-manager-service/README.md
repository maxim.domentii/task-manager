# Tasm Manager Service
Task Manager Service web application built with Java 8 and Spring Boot 2.

It has a manager capacity and different strategies for adding a new process.
These properties are given at start-up time. 
Possible values for strategies are DEFAULT, FIFO and PriorityBased.
If no property is given it will start by default with capacity=10 and strategy=DEFAULT.

It includes also unit tests under src/test package. 

## Build
To build the package run: 
```shell
mvn clean install
```

## Run
To start the application run the command bellow. 
Any combination of the arguments is possible. 
Here listed only some examples:
```shell
    mvn spring-boot:run
    # or
    mvn spring-boot:run -Dspring-boot.run.arguments="--task.manager.capacity=5"
    # or
    mvn spring-boot:run -Dspring-boot.run.arguments="--task.manager.factory-method=FIFO"
    # or
    mvn spring-boot:run -Dspring-boot.run.arguments="--task.manager.capacity=5 --task.manager.factory-method=PriorityBased"
```

## Usage
You may try out the service without UI component. 
For this use the postman collection from the root directory of this package
or use the following curl commands:
```shell
## To list the processes
curl -X GET
  http://localhost:8080/task-manager/processes
  -H 'accept: application/json'
  -H 'content-type: application/json'

# or with sorting params
 curl -X GET
  'http://localhost:8080/task-manager/processes?property=CREATION_TIME&direction=DESC'
  -H 'accept: application/json'
  -H 'content-type: application/json'

# possible values for property param: CREATION_TIME, PRIORITY, ID
# possible values for direction param: ASC, DESC

## To add a new process
curl -X POST
  http://localhost:8080/task-manager/process
  -H 'accept: application/json'
  -H 'content-type: application/json'
  -d '{
	"id": "pid-1",
	"priority": "LOW"
}'

## To kill a process by PID
curl -X DELETE
  http://localhost:8080/task-manager/processes/pid-1
  -H 'accept: application/json'
  -H 'content-type: application/json'
  
## To kill processes by priority
curl -X DELETE
  'http://localhost:8080/task-manager/processes?priority=LOW'
  -H 'accept: application/json'
  -H 'content-type: application/json'
  
# possible values for priority param: LOW, MEDIUM, HIGH
  
## To kill all processes
curl -X DELETE
  http://localhost:8080/task-manager/processes
  -H 'accept: application/json'
  -H 'content-type: application/json'
```